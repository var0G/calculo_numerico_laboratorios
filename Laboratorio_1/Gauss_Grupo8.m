function x=Gauss_pivoteo_Grupo8(A,b)
  [n,m]=size(A);

  if n~=m  %realiza la verificacion si la matriz es cuadrada
    x=[];
    return;
  end

  a=[A,b];  %matriz aumentada
  for y=1:n
    [f,c]=max(abs(a(y:n,y)));  %Pivoteo por las filas
    c=c+y-1;
    t=a(y,y:n+1);
    a(y,y:n+1)=a(c,y:n+1);
    a(c,y:n+1)=t;

    if abs(a(y,y))<1.0e-10    % comprueba si el dividor es 0, no hay solucion
      x=[];
      return;
    end

    a(y,y:n+1)=a(y,y:n+1)/a(y,y);     %normalizar la fila e
    for i=y+1:n                       %Reducir otras filas
      a(i,y:n+1)=a(i,y:n+1)-a(i,y)*a(y,y:n+1);
    end
  end

  x(n,1)=a(n,n+1);              %solucion del sistema triangular
  for i=n-1:-1:1      
    x(i,1)=a(i,n+1)-a(i,i+1:n)*x(i+1:n,1);
  end

  A
  b
  rref(a)

endfunction
