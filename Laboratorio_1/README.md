# Laboratorio 1

En este laboratorio se  debieron resolver dos problemas, uno de circuitos y otro  
de un arco enrejado para llegar a las ecuaciones y convertir estas en matrices,   
para luego aplicar los algoritmos propuestos en clases y otros opcionales,   
y dar respuestas a las distintas problemáticas presentadas. 

* Para este laboratorio se programo y se trabajo con
  [Octave](https://www.gnu.org/software/octave/index)  el cual es un lenguaje de
programación con su propio IDE, ademas es compatible con  matlab y de
libre distribución ya que esta bajo la licencia GNU. 
