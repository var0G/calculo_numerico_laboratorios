function x=Jacobi_Algebraico_Grupo8(A,b,N)
  % resoluci�n de sistemas lineales por Gauss-Seidel
  % A: matriz del sistema
  % b: t�rmino independiente
  % E: tolerancia (e^{-8})
  % N: n�mero de iteraciones

  [m,n]=size(A);
  x=zeros(n,1);
  E=1e-8;

  if m~=n
    'Requisitos no presentados: Se necesita una matriz cuadrada'
    return
  endif

  for i=1:m
    if A(i,i)==0
      'Elementos principales nulos'
      return
    endif 
  endfor

%operaci�nes previas
  Fact=(inv(diag(diag(A))))*((diag(diag(A))-tril(A))-(triu(A)-diag(diag(A))));
  Sum=(inv(diag(diag(A))))*b;
  y=x;

%iteraciones
  for k=1:N
      y=Fact*x+Sum;

      if max(abs(y-x))<E
        'n� de iteraciones: ',k
        return
      endif 

    x=y;
  endfor

endfunction
