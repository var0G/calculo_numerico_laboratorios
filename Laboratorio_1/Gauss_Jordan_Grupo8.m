function [x] = Gauss_Jordan_Grupo8(matriz_A,matriz_b) 

  matriz_A
  matriz_b

  m_Aumentada = [matriz_A, matriz_b]
  [m,n]=size(m_Aumentada);

  x = zeros(m,1); 

  for j=1:m-1 
    for z=2:m 
      if m_Aumentada(j,j)==0
        aux=m_Aumentada(1,:);
        m_Aumentada(1,:)=m_Aumentada(z,:);
        m_Aumentada(z,:)=aux;
      end 
    end

    for i=j+1:m
      m_Aumentada(i,:)=m_Aumentada(i,:)-m_Aumentada(j,:)*(m_Aumentada(i,j)/m_Aumentada(j,j));
    end 
  end
  
  for j=m:-1:2
    for i=j-1:-1:1
      m_Aumentada(i,:)=m_Aumentada(i,:)-m_Aumentada(j,:)*(m_Aumentada(i,j)/m_Aumentada(j,j));
    end 
  end

  for s=1:m
    m_Aumentada(s,:)=m_Aumentada(s,:)/m_Aumentada(s,s);
    x(s)=m_Aumentada(s,n);
  end

  m_Aumentada

endfunction
