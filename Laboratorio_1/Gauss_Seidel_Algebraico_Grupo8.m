function x=Gauss_Seidel_Algebraico_Grupo8(A,b,N)
  % resoluci�n de sistemas lineales por Gauss-Seidel
  % A: matriz del sistema
  % b: t�rmino independiente
  % E: tolerancia (e^{-8})
  % N: n�mero de iteraciones

  [m,n]=size(A);
  x=zeros(n,1);
  E=1e-8;

  if m~=n
  'Requisitos no presentados: Se necesita una matriz cuadrada'
  return
  endif

  for i=1:m
    if A(i,i)==0
      'Elementos principales nulos'
      return
    endif
  endfor

  y=x;
%iteraci�n
  for k=1:N
    y(1)=(b(1)-A(1,2:n)*x(2:n))/A(1,1);

    for i=2:n-1
      y(i)=(b(i)-A(i,1:i-1)*y(1:i-1)-A(i,i+1:n)*x(i+1:n))/A(i,i);
    endfor

    y(n)=(b(n)-A(n,1:n-1)*y(1:n-1))/A(n,n);

    if max(abs(y-x))<E
      'n� de iteraciones: ',k
      return
    endif 

    x=y;
  endfor

endfunction
