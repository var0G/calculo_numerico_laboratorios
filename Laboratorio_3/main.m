clc
clear
close all

%constantes del problema
masa_1 = 4;
masa_2 = 6;
largo_1 = 2;
largo_2 = 3; 
g = 9.81; 
alfa_1 = pi/3;
alfa_2 = pi/4;
v_1 = 0;
v_2 = 0;
t = 0;
t_max = 20;

%angulos
alfa = masa_2/masa_1;
beta = largo_2/largo_1;
gamma = g/largo_1;

[T, X] = ode45(@(t,x)Pendulo_doble_Grupo_8(t,x,alfa,beta,gamma), [0, t_max], [alfa_1, v_1, alfa_2, v_2])
sol = ode45(@(t,x)Pendulo_doble_Grupo_8(t,x,alfa,beta,gamma), [0, t_max], [alfa_1, v_1, alfa_2, v_2])

figure(1)
plot(sol.x, sol.y);

legend('\theta_2','d\theta_2/dt','\theta_1','d\theta_1/dt')
title('Estado de las variables')
xlabel('Tiempo (s)')
ylabel('solucion radianes ')
drawnow;

figure(2)
for i=1:length(T) 

    posicion_m1(1) = largo_1*sin(X(i,1));
    posicion_m1(2) = -largo_1*cos(X(i,1));
    posicion_m2(1) = posicion_m1(1) + largo_2*sin(X(i,3));
    posicion_m2(2) = -largo_1*cos(X(i,1)) - largo_2*cos(X(i,3));
    
    subplot(1,1,1)

    plot( [0 posicion_m1(1)], [0 posicion_m1(2)], 'k' );
    hold on;

    plot( [posicion_m1(1) posicion_m2(1)], [posicion_m1(2) posicion_m2(2)], 'k-' );
    hold on;

    plot( posicion_m1(1), posicion_m1(2), 'go-', 'markersize', masa_1*8, 'markerfacecolor', 'green' ) 
    hold on;

    plot( posicion_m2(1), posicion_m2(2),'ro', 'markersize', masa_2*8, 'markerfacecolor','red' ) 

    %constantes para los margenes de los ejes en plot 
    xlim ([(-largo_1-largo_2) (largo_1+largo_2)]);    
    ylim ([(-largo_1-largo_2) (largo_1+largo_2)]);

    axis(xlim, ylim);
    title(['Tiempo=', num2str(T(i),2), 's'])
    axis equal;
    drawnow;
    hold off;
end
