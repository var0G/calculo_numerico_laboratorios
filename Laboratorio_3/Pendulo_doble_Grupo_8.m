function dx = Pendulo_doble_Grupo_8(t,x,alfa,beta,gamma)
    dx = zeros(4,1);
    dx(1) = x(2);
    dx(2) = -((1+alfa)*gamma*sin(x(1)) + alfa*beta*(x(4))^2*sin(x(1)-x(3)) + alfa*cos(x(1)-x(3))*(x(2)^2*sin(x(1)-x(3))-gamma*sin(x(3)))) / (1+alfa*sin(x(1)-x(3))^2); 
    dx(3) = x(4);
    dx(4) = (((1+alfa)*(x(2)^2*sin(x(1)-x(3)))-gamma*sin(x(3)))+cos(x(1)-x(3))*((1+alfa)*gamma*sin(x(1))+alfa*beta*x(4)^2*sin(x(1)-x(3)))) / (beta*(1+alfa*sin(x(1)-x(3))^2));
endfunction
