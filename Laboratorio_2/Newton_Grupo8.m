function Newton_Grupo8(f,g,x0,tol,N)
    f = f
    g = g;
    tol = tol;
    x0 = x0;
    [solucion,iteraciones] = Newton(f, g, x0, tol,N);
    if iteraciones > 0   % Es posible encontrar una solucion
        fprintf('Numero de iteraciones: %d\n', 1 + iteraciones);
        fprintf('Posible raiz: %f\n', solucion)
    else
        fprintf('Ejecucion abortada, no se realizaron iteraciones\n')
    endif
endfunction

function [solucion, iteraciones] = Newton(f, g, x0, tol,N)
    x(1) = x0;
    i = 1;
    f_valor=f(x(i));
    while abs(f_valor) > tol && i < N
        try
            x(i+1) = x(i) - (f_valor)/g(x(i));
        catch
            fprintf('Error: Derivada = 0 en x = \n', x(i))
            break
        end_try_catch
        i = i + 1;
        f_valor=f(x(i));
    endwhile
    % Maximo de iteraciones alcanzado o raiz encontrada
    solucion = x(i);
    iteraciones = i;
    figure
    subplot(2,1,1);
    plot(x, 'bo-');
endfunction