function raiz=Biseccion_Grupo8(f,limsup,liminf,tol)
  % Resolucion de la ecuacion f(x)=0 mediante el algoritmo de biseccion.
  % f: Funcion cuya raiz se desea calcular
  %   (Para no causar problemas con el código, debe definirse previamente
  %   mediante un comando 'inline')
  % limsup: Extremo inferior del intervalo inicial
  % liminf: Extremo superior del intervalo inicial
  % tol: tolerancia

  if limsup>liminf 
    % Comprobacion y correcion del orden de los extremos del intervalo
    'Limites introducidos en el orden contrario'
    a=limsup;
    limsup=liminf;
    liminf=a;
  endif
  i=1;
  biseccion(1)=(limsup+liminf)/2;
  if abs(liminf-limsup)<tol
    raiz=biseccion(i);
  else
    while((f(biseccion(i))~=0)&&abs(liminf-limsup)>tol)
      if f(biseccion(i))*f(liminf)<0
        limsup=biseccion(i);
      else
        liminf=biseccion(i);
      endif
      i=i+1;
      biseccion(i)=(limsup+liminf)/2;
    endwhile
    raiz=biseccion(i);
  endif
  figure
  subplot(2,1,1);
  plot(biseccion, 'bo-');
  biseccion;
endfunction
