function Secante_Grupo8(f,x0,x1,tol,N)
    [solucion,iteraciones] = secante(f, x0, x1, tol,N);
    if iteraciones > 0   % Es posible enirar una solucion
        fprintf('Numero de iteraciones: %d\n', 2 + iteraciones);
        fprintf('Posible raiz: %f\n', solucion)
    else
        fprintf('Ejecucion abortada, no se requieren iteraciones\n')
    endif
endfunction

function [solucion,iteraciones] = secante(f, x0, x1, tol,N)
    f_x0 = f(x0);
    f_x1 = f(x1);
    i = 1;
    while abs(f_x1) > tol && i <= N
        try
            denominador = (f_x1 - f_x0)/(x1 - x0);
            x(i) = x1 - (f_x1)/denominador;
        catch
            fprintf('Error: Denominador cero en x = \n', x1)
            break
        end
        x0 = x1;
        x1 = x(i);
        f_x0 = f_x1;
        f_x1 = f(x1);
        i = i + 1;
    endwhile
    % Maximo de iteraciones alcanzado o raiz encontrada
    if abs(f_x1) > tol
        i = -1;
    % Porque el while suma una �ltima vez antes de salir del ciclo
    endif
    solucion = x1;
    iteraciones = i;
    figure
    subplot(2,1,1);
    plot(x, 'bo-');
endfunction